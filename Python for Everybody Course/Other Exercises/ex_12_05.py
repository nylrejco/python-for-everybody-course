# To run this, download the BeautifulSoup zip file
# http://www.py4e.com/code3/bs4.zip
# and unzip it in the same directory as this file

import urllib.request, urllib.parse, urllib.error
from bs4 import BeautifulSoup
import ssl

# Ignore SSL certificate errors
ctx = ssl.create_default_context()
ctx.check_hostname = False
ctx.verify_mode = ssl.CERT_NONE

url = input('Enter URL: ')
html = urllib.request.urlopen(url, context=ctx).read()
soup = BeautifulSoup(html, 'html.parser')

count = input('Enter count:')
pos = input('Enter position:')
count = int(count)
pos = int(pos)-1
links = []

ref = soup('a')
for i in range(count):
    link = ref[pos].get('href', None)
    print('Retrieving:', link)
    links.append(ref[pos].contents[0])
    html = urllib.request.urlopen(link, context=ctx).read()
    soup = BeautifulSoup(html, 'html.parser')
    ref = soup('a')
    #link = ref[pos].get('href', None)
    #print(tag.get('href', None))
print(links[-1])
