# Use the file name mbox-short.txt as the file name
fname = input("Enter file name: ")
fh = open(fname)
count = 0
val = 0
for line in fh:
    if not line.startswith("X-DSPAM-Confidence:") : continue
    value = line[19:]
    value = float(value.rstrip())
    val = val + value
    count = count + 1

tot = val/count
print('Average spam confidence:',tot)
#print("Done")
