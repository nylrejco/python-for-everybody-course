import re
hand = open('regex_sum_1061325.txt')
allnum = list()
total = 0
for line in hand:
    line = line.rstrip()
    x = re.findall('[0-9]+', line)
    if len(x) > 0:
        allnum = x+allnum

for num in allnum:
    total = int(num) + total

print(total)
