import urllib.request, urllib.parse, urllib.error
import json
import ssl
#api_key = False
# If you have a Google Places API key, enter it here
# api_key = 'AIzaSy___IDByT70'
# https://developers.google.com/maps/documentation/geocoding/intro
#if api_key is False:
#    api_key = 42
#    serviceurl = 'http://py4e-data.dr-chuck.net/json?'
#else :
#    serviceurl = 'https://maps.googleapis.com/maps/api/geocode/json?'
# Ignore SSL certificate errors
ctx = ssl.create_default_context()
ctx.check_hostname = False
ctx.verify_mode = ssl.CERT_NONE



while True:
    address = input('Enter location: ')
    if len(address) < 1: break


    print('Retrieving', address)
    uh = urllib.request.urlopen(address, context=ctx)
    data = uh.read().decode()
    print('Retrieved', len(data), 'characters')
    info = json.loads(data)
    #print(json.dumps(info, indent=4))

    count = 0
    total = 0
    for nums in info['comments']:
        num = nums['count']
        total = total + int(num)
        count = count + 1
    print('Count:', count)
    print('Sum:', total)
