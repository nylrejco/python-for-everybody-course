fname = input("Enter file name: ")
if len(fname) < 1 : fname = "mbox-short.txt"

fh = open(fname)
dc = dict()
for line in fh:
    words = line.split()
    if len(words) == 0 : continue
    if words[0] != 'From': continue
    hour = words[5]
    hours = hour[0:2]
    dc[hours] = dc.get(hours,0)+1

lst=list()
for hr,ct in dc.items():
    lst.append((hr,ct)) #tuple is (ht,ct)
    

lst.sort()
for hr,ct in lst:
    print(hr,ct)
